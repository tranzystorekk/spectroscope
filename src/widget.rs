use ratatui::layout::Rect;
use ratatui::style::Style;
use ratatui::symbols;
use ratatui::symbols::bar::Set;
use ratatui::widgets::{Block, Widget};

pub struct BarSpectrum<'a> {
    block: Option<Block<'a>>,
    bar_width: u16,
    bar_gap: u16,
    bar_set: Set,
    bar_base: Option<&'static str>,
    bar_style: Style,
    bar_base_style: Style,
    style: Style,
    max: Option<u64>,
    data: &'a [u64],
}

impl<'a> BarSpectrum<'a> {
    pub fn block(mut self, block: Block<'a>) -> Self {
        self.block = Some(block);
        self
    }

    pub fn bar_width(mut self, width: u16) -> Self {
        self.bar_width = width;
        self
    }

    pub fn bar_gap(mut self, gap: u16) -> Self {
        self.bar_gap = gap;
        self
    }

    pub fn bar_set(mut self, bar_set: Set) -> Self {
        self.bar_set = bar_set;
        self
    }

    pub fn bar_base(mut self, base: &'static str) -> Self {
        self.bar_base = Some(base);
        self
    }

    pub fn bar_style(mut self, style: Style) -> Self {
        self.bar_style = style;
        self
    }

    pub fn bar_base_style(mut self, style: Style) -> Self {
        self.bar_base_style = style;
        self
    }

    pub fn style(mut self, style: Style) -> Self {
        self.style = style;
        self
    }

    pub fn data(mut self, data: &'a [u64]) -> Self {
        self.data = data;
        self
    }

    pub fn max(mut self, max: u64) -> Self {
        self.max = Some(max);
        self
    }
}

pub fn bar_capacity(screen: Rect, bar_width: u16, bar_gap: u16) -> u16 {
    let column_width = bar_width + bar_gap;

    screen.width / column_width + (screen.width % column_width >= bar_width) as u16
}

impl<'a> Default for BarSpectrum<'a> {
    fn default() -> Self {
        Self {
            block: None,
            bar_width: 1,
            bar_gap: 1,
            bar_set: symbols::bar::NINE_LEVELS,
            bar_base: None,
            bar_style: Style::default(),
            bar_base_style: Style::default(),
            style: Style::default(),
            max: None,
            data: &[],
        }
    }
}

impl<'a> Widget for BarSpectrum<'a> {
    fn render(mut self, area: ratatui::layout::Rect, buf: &mut ratatui::buffer::Buffer) {
        buf.set_style(area, self.style);

        let spectrum_area = self
            .block
            .take()
            .map(|b| {
                let inner_area = b.inner(area);
                b.render(area, buf);
                inner_area
            })
            .unwrap_or(area);

        if spectrum_area.height < 1 {
            return;
        }

        if self.data.is_empty() {
            return;
        }

        let bar_height = if self.bar_base.is_some() {
            spectrum_area.height - 1
        } else {
            spectrum_area.height
        };

        let max = self
            .max
            .unwrap_or_else(|| self.data.iter().copied().max().unwrap_or_default());

        let column_width = self.bar_width + self.bar_gap;
        let capacity = bar_capacity(spectrum_area, self.bar_width, self.bar_gap);
        let bars_rendered = std::cmp::min(self.data.len() as u16, capacity);
        let spectrum_width = bars_rendered * column_width - self.bar_gap;
        let left_pad = (spectrum_area.width - spectrum_width) / 2;

        // draw spectrum bars
        for j in (0..bar_height).rev() {
            let values = self
                .data
                .iter()
                .copied()
                .take(bars_rendered as usize)
                .map(|v| v * bar_height as u64 * 8 / std::cmp::max(max, 1) as u64);
            for (i, v) in values.enumerate() {
                let tmp = bar_height - j - 1;
                let remainder = u64::saturating_sub(v, tmp as u64 * 8);
                let symbol = match remainder {
                    0 => self.bar_set.empty,
                    1 => self.bar_set.one_eighth,
                    2 => self.bar_set.one_quarter,
                    3 => self.bar_set.three_eighths,
                    4 => self.bar_set.half,
                    5 => self.bar_set.five_eighths,
                    6 => self.bar_set.three_quarters,
                    7 => self.bar_set.seven_eighths,
                    _ => self.bar_set.full,
                };

                for x in 0..self.bar_width {
                    buf[(
                        spectrum_area.left() + left_pad + i as u16 * column_width + x,
                        spectrum_area.top() + j,
                    )]
                        .set_symbol(symbol)
                        .set_style(self.bar_style);
                }
            }
        }

        let Some(base_char) = self.bar_base else {
            return;
        };

        // draw base lines
        for i in 0..bars_rendered {
            for x in 0..self.bar_width {
                buf[(
                    spectrum_area.left() + left_pad + i * column_width + x,
                    spectrum_area.bottom() - 1,
                )]
                    .set_symbol(base_char)
                    .set_style(self.bar_base_style);
            }
        }
    }
}
