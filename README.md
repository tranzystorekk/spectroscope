# spectroscope

![Visualizer demo](assets/visualizer_demo.gif)

## About

A demo project for a TUI sound visualizer,
based on [ratatui](https://github.com/tui-rs-revival/ratatui) and [cava](https://github.com/karlstav/cava).

## Usage

### Setup ALSA loopback device

Currently, `spectroscope` is hardwired to read a default ALSA loopback device.

This typically requires you to load the `snd_aloop` kernel module, either manually or automatically.

### Configure your music player to duplicate output to loopback

This is typically done easily with graphical patchbay applications like `qpwgraph`, `helvum`;
identify your player's output and the loopback playback endpoint there and connect them.

### Build and run

Clone the git repo and run:

```console
cargo run --release --locked -- <params>
```
